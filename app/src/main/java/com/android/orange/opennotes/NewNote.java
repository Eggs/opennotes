package com.android.orange.opennotes;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

public class NewNote extends AppCompatActivity {

    private EditText newNoteTitle;
    private EditText newNoteContent;

    private String noteFileName;
    private Note loadedNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_note);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        newNoteTitle = (EditText) findViewById(R.id.new_note_check_list_title);
        newNoteContent = (EditText) findViewById(R.id.new_note_content);

        noteFileName = getIntent().getStringExtra("NOTE_FILE");

        if (noteFileName != null && !noteFileName.isEmpty()) {
            loadedNote = Utilities.getSavedNote(this, noteFileName);
            newNoteTitle.setText(loadedNote.getTitle());
            newNoteContent.setText(loadedNote.getContent());

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_note_new, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.new_note_save:
                saveNote();
                return true;
            case R.id.new_note_delete:
                deleteNote();
                return true;
            case android.R.id.home:
                this.finish();
        }

        return true;
    }

    private void saveNote(){
        Note note;
        if (loadedNote == null) {
            note = new Note(System.currentTimeMillis(), newNoteTitle.getText().toString(), newNoteContent.getText().toString());
        } else {
            note = new Note(loadedNote.getDateTime(), newNoteTitle.getText().toString(), newNoteContent.getText().toString());
        }

        if(Utilities.saveNote(this, note)){
            Toast.makeText(this, "Note saved!", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Oops! Something went wrong D:", Toast.LENGTH_SHORT).show();
        }
        finish(); // Closes activity and takes user back to last activity.

    }

    private void deleteNote(){
        if (noteFileName != null) {
            Utilities.deleteNote(getApplicationContext(), noteFileName);
            Toast.makeText(NewNote.this, "Deleted!", Toast.LENGTH_SHORT).show();
            finish();
        } else {
            Toast.makeText(this, "Please save first before deleting.", Toast.LENGTH_SHORT).show();
        }
    }
}
