/*
    opennotes
    Copyright (C) 2017  Egg

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package com.android.orange.opennotes;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MainActivity extends AppCompatActivity {

    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setImageResource(R.drawable.ic_create_black_24dp);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NewNote.class);
                startActivity(intent);
            }
        });

        listView = findViewById(R.id.main_activity_list_view);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_main_menu_new_note:
                Intent intent = new Intent(MainActivity.this, NewNote.class);
                startActivity(intent);
                return true;
            case R.id.action_main_menu_new_note_check_list:
                Intent check_list_intent = new Intent(MainActivity.this, NewNoteCheckList.class);
                startActivity(check_list_intent);
                return true;
            case R.id.action_main_menu_about:
                Intent intent_about = new Intent(this, About.class);
                startActivity(intent_about);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

        @Override
        protected void onResume () {
            super.onResume();

            listView.setAdapter(null);

            final ArrayList<Note> notes = Utilities.getAllSavedNotes(this);

            Comparator<Note> comparator = new Comparator<Note>() {
                @Override
                public int compare(Note note, Note t1) {
                    return note.getDateTimeFormatted(MainActivity.this).compareTo(t1.getDateTimeFormatted(MainActivity.this));
                }
            };
            Collections.sort(notes, comparator.reversed());

            if (notes != notes || notes.size() == 0) {
                Toast.makeText(this, "No saved notes found.", Toast.LENGTH_SHORT).show();
                return;
            } else {
                Collections.sort(notes, new CustomComparator());
                final NoteAdapter noteAdapter = new NoteAdapter(this, R.layout.item_note, notes);

                listView.setAdapter(noteAdapter);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, final int position, long l) {
                        AlertDialog.Builder openNoteDialog = new AlertDialog.Builder(MainActivity.this);
                        openNoteDialog.setTitle("Choose how to open note");
                        openNoteDialog.setPositiveButton("As note", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                String fileName = ((Note) listView.getItemAtPosition(position)).getDateTime() + Utilities.FILE_EXTENSION;
                                Intent intent = new Intent(getApplicationContext(), NewNote.class);

                                intent.putExtra("NOTE_FILE", fileName);
                                startActivity(intent);
                            }
                        });

                        openNoteDialog.setNegativeButton("As checklist", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                String fileName = ((Note) listView.getItemAtPosition(position)).getDateTime() + Utilities.FILE_EXTENSION;
                                Intent intent = new Intent(getApplicationContext(), NewNoteCheckList.class);

                                intent.putExtra("NOTE_FILE", fileName);
                                startActivity(intent);
                            }
                        });
                        openNoteDialog.setCancelable(true);
                        openNoteDialog.create().show();


                    }
                });

               listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int position, long l) {
                        AlertDialog.Builder deleteAlert  = new AlertDialog.Builder(MainActivity.this);
                        deleteAlert.setTitle("Delete note");
                        deleteAlert.setMessage("Really delete " + (((Note) listView.getItemAtPosition(position)).getTitle() + "?"));
                        deleteAlert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                Utilities.deleteNote(getApplicationContext(), ((Note) listView.getItemAtPosition(position)).getDateTime() + Utilities.FILE_EXTENSION);
                                //listView.invalidate()
                                noteAdapter.notifyDataSetChanged();
                                Toast.makeText(MainActivity.this, "Note deleted!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        deleteAlert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        });
                        deleteAlert.setCancelable(true);
                        deleteAlert.create().show();

                        return true;
                    }
                });
            }
        }
}
