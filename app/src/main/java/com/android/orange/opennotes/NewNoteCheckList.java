package com.android.orange.opennotes;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class NewNoteCheckList extends AppCompatActivity {

    /* Delcare variable to reference ListView UI component. */
    private ListView listView;

    /* Array of strings to store individual items of checklist.
     * This will later be used as content in the note */
    List<String> checkListItems = new ArrayList<>();

    /* UI components need an adapter to tell it how to display data.
     * DATA -> ADAPTER (how to display) -> VIEW */
    ArrayAdapter<String> checkListAdapter;

    private EditText newNoteTitle;

    private String noteFileName;
    private Note loadedNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_note_check_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        noteFileName = getIntent().getStringExtra("NOTE_FILE");

        newNoteTitle = (EditText) findViewById(R.id.new_note_check_list_title);

        if (noteFileName != null && !noteFileName.isEmpty()) {
            loadedNote = Utilities.getSavedNote(this, noteFileName);
            newNoteTitle.setText(loadedNote.getTitle());
            //checkListItems = Arrays.asList(loadedNote.getContent().split(","));
            List<String> loadedItemList = new ArrayList<>(Arrays.asList(loadedNote.getContent().split(",")));
            checkListItems = loadedItemList;

        }

        /* Set listView to the UI component from content_new_note_check_list.xml */
        listView = findViewById(R.id.new_note_check_list_list_view);

        /* Initialise the adapter with context, a layout - in this case an Android default - and the array. */
        checkListAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_single_choice, checkListItems);

        /* Finally, tell listView which adapter to use */
        listView.setAdapter(checkListAdapter);

        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        newNoteTitle = (EditText) findViewById(R.id.new_note_check_list_title);

        noteFileName = getIntent().getStringExtra("NOTE_FILE");


        /* On click of list item */
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                listView.setItemChecked(position, true);
                /* Remove the item from the list */
                checkListItems.remove(position);
                /* Tell the adapter we've changed the list */
                checkListAdapter.notifyDataSetChanged();

                listView.setItemChecked(position, false);

            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_note_checklist, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        final EditText editText = new EditText(this);

        switch(id){
            case R.id.action_new_note_check_list_add:
                AlertDialog.Builder checkListDialog = new AlertDialog.Builder(this);
                checkListDialog.setTitle("Add new item");
                checkListDialog.setView(editText);
                checkListDialog.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        checkListAdapter.add(editText.getText().toString());
                        //checkListItems.add();
                        checkListAdapter.notifyDataSetChanged();
                        Toast.makeText(NewNoteCheckList.this, "Added new checklist item", Toast.LENGTH_SHORT).show();
                    }

                });
                checkListDialog.setCancelable(true);
                checkListDialog.create().show();
                return true;
            case R.id.action_new_note_check_list_save:
                saveNote();

            case android.R.id.home: /* For action bar back button */
                 this.finish();
                 return true;
            }

        return super.onOptionsItemSelected(item);
        }

    private void saveNote(){
        Note note;

        if (loadedNote == null) {
            /* Convert the checklist items to string for saving */
            note = new Note(System.currentTimeMillis(), newNoteTitle.getText().toString(), checkListItems.toString());
        } else {
            note = new Note(loadedNote.getDateTime(), newNoteTitle.getText().toString(), checkListItems.stream().collect(Collectors.joining(",")));
        }

        if(Utilities.saveNote(this, note)){
            Toast.makeText(this, "Note saved!", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Oops! Something went wrong D:", Toast.LENGTH_SHORT).show();
        }
        finish(); // Closes activity and takes user back to last activity.

    }

    }

