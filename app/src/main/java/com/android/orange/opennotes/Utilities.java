package com.android.orange.opennotes;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by orange on 12/1/17.
 */

public class Utilities {
    public static final String FILE_EXTENSION = ".bin";

    public static boolean saveNote(Context context, Note note){
        String fileName = note.getDateTime() + FILE_EXTENSION;

        FileOutputStream fileOutputStream;
        ObjectOutputStream objectOutputStream;

        try {
            fileOutputStream = context.openFileOutput(fileName, context.MODE_PRIVATE); // Open fos and set file to be save in internal storage.
            objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(note);
            objectOutputStream.close();
            fileOutputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }


    public static ArrayList<Note> getAllSavedNotes(Context context) {
        ArrayList<Note> notes = new ArrayList<>();

        File filesDir = context.getFilesDir();
        ArrayList<String> noteFiles = new ArrayList<>();

        for (String file : filesDir.list()) {
            if (file.endsWith(FILE_EXTENSION)){
                noteFiles.add(file);
            }
        }

        FileInputStream fileInputStream;
        ObjectInputStream objectInputStream;

        for (int i = 0; i < noteFiles.size(); i++) {
            try{
                fileInputStream = context.openFileInput(noteFiles.get(i));
                objectInputStream = new ObjectInputStream(fileInputStream);

                notes.add((Note) objectInputStream.readObject());

                fileInputStream.close();
                objectInputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        return notes;
    }

    public static Note getSavedNote(Context context, String fileName) {
        File file = new File(context.getFilesDir(),fileName);
        Note note;

        if (file.exists()) {
            FileInputStream fileInputStream;
            ObjectInputStream objectInputStream;

            try {
                fileInputStream = context.openFileInput(fileName);
                objectInputStream = new ObjectInputStream(fileInputStream);

                note = (Note) objectInputStream.readObject();

                fileInputStream.close();
                objectInputStream.close();

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
            return note;
        }
        return null;
    }

    public static Note getSavedCheckList(Context context, String fileName){
        File file = new File(context.getFilesDir(), fileName);
        Note note;

        if (file.exists()) {
            FileInputStream fileInputStream;
            ObjectInputStream objectInputStream;

            try {
                fileInputStream = context.openFileInput(fileName);
                objectInputStream = new ObjectInputStream(fileInputStream);

                note = (Note) objectInputStream.readObject();

                fileInputStream.close();
                objectInputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
            return note;

        }
        return null;
    }

    public static boolean deleteNote(Context context, String fileName) {
        File file = new File(context.getFilesDir(), fileName);
        if (file.exists()) {
            file.delete();
            return true;
        } else {
            return false;
        }
    }
}
